import {CellsPage} from '@cells/cells-page';
import {html} from 'lit-element';
import '@cells-components/cells-template-paper-drawer-panel';
import 'unsplash-finder/src/unsplash-finder.js';

class FinderPage extends CellsPage {
	static get is() {
		return 'finder-page';
	}

	static get properties() {
		return {
			headerTitle: { type: String }
		};
	}

	constructor() {
		super();
		this.headerTitle = 'undefined'
	}

	static get shadyStyles() {
		return `
		unsplash-finder {
			width: 100%;
		}

		#main-container {
			font-family: sans-serif;
		}
		
		#header-container {
			display: flex;
	    justify-content: space-between;
	    align-items: center;
		}

		#home-button {
			font-size: 13px;
			padding: 0.5rem;
	    padding-left: 1rem;
	    padding-right: 1rem;
	    border-radius: 5px;
		}
		`;
	}

	render() {
		return html`
		<style>${this.constructor.shadyStyles}</style>
		<cells-template-paper-drawer-panel mode="seamed">
			<div slot="app__main" id="main-container">
				<div id="header-container">
					<h2>Unsplash Finder</h2>
					<button id="home-button" @click="${this.changePage}">Home</button>
				</div>
				<unsplash-finder></unsplash-finder>
			</div>
		</cells-template-paper-drawer-panel>
		`;
	}

	move() {
		this.navigate('home', {title: 'This is a HOME title'});
	}

	changePage() {
		this.navigate('home');
	}

}

window.customElements.define(FinderPage.is, FinderPage);