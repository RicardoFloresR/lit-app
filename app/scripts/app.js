(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
	// FileName: URL
      'home': '/',
      'another': '/another',
      'finder': '/finder'
    }
  });
}(document));
